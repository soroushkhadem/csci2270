#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

int insertIntoSortedArray(int myArray[], int numEntries, int newValue);

int main (int argc, char* argv[]) {
  if (argc < 2) {
    cout << "Need to enter a filename" << endl;
    return 1;
  }
  cout << "Attempting to read file: " << argv[1] << endl;

  int myArray[100];
  int elementCount = 0;

  ifstream inFile;
  string data;

  inFile.open(argv[1]);
    if (inFile.is_open()) {
    int inputVal;
    while(getline(inFile, data)) {
      stringstream ss(data);
      ss >> inputVal;
      elementCount = insertIntoSortedArray(myArray, elementCount, inputVal);
      // printing
      for (int i=0;i<elementCount;i++) {
        cout << myArray[i];
        if (i<elementCount - 1) {
          cout << ", ";
        }
        else {
          cout << "\n";
        }
      }

    }
    inFile.close();
  }
  else {
    cout << "Error opening file" << endl;
    return 1;
  }

  return 0;

}

int insertIntoSortedArray(int myArray[], int numEntries, int newValue) {

  int elementSpot=-1;

  for (int i=0; i<numEntries; i++) {
    if (newValue <= myArray[i]) {
      elementSpot = i;
      break;
    }
  }

  if (elementSpot == -1) {
    elementSpot = numEntries;
  }


  for (int i=numEntries;i>elementSpot;i--) {
    myArray[i] = myArray[i-1];
  }

  myArray[elementSpot] = newValue;

  numEntries++;
  return numEntries;
}
