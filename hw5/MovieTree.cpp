#include "MovieTree.hpp"
#include <iostream>
#include <string>

using namespace std;

MovieTree::MovieTree(){
    root = nullptr;
}

MovieTree::~MovieTree(){

}

MovieNode* addMovieHelper(MovieNode* curr, int ranking, string title, int releaseYear, int quantity) {
    if (curr == nullptr) return new MovieNode(ranking, title, releaseYear, quantity);

    if (title.compare(curr->title) < 0) curr->leftChild = addMovieHelper(curr->leftChild, ranking, title, releaseYear, quantity);
    else curr->rightChild = addMovieHelper(curr->rightChild, ranking, title, releaseYear, quantity);

    return curr;
}

void MovieTree::addMovieNode(int ranking, string title, int releaseYear, int quantity) {

    if(root==nullptr) {
        root = new MovieNode(ranking, title, releaseYear, quantity);
        return;
    }

    root = addMovieHelper(root, ranking, title, releaseYear, quantity);

}


void printMoviesHelper(MovieNode* curr){
    if (curr) {
        printMoviesHelper(curr->leftChild);
        cout << "Movie: " << curr->title << " " << curr->quantity << endl;
        printMoviesHelper(curr->rightChild);
    }
}

void MovieTree::printMovieInventory() {
    printMoviesHelper(root);
}

MovieNode* searchHelper(MovieNode* root, string title) {
    if (root == nullptr || title.compare(root->title) == 0) return root;

    if (title.compare(root->title) < 0) return searchHelper(root->leftChild, title);

    return searchHelper(root->rightChild, title);

}

void MovieTree::findMovie(string title) {
    MovieNode* found_movie = searchHelper(root, title);
    if (found_movie == nullptr) {cout << "Movie not found." << endl; return;}

    cout << "Movie " << "'" << found_movie->title << "' Info:" <<endl;
    cout << "================================" << endl;
    cout << "Ranking : " << found_movie->ranking << endl;
	cout << "Title   : " << found_movie->title << endl;
	cout << "Year    : " << found_movie->year << endl;
	cout << "Quantity: " << found_movie->quantity << endl;
    return;

}

void MovieTree::rentMovie(string title) {
    MovieNode* found_movie = searchHelper(root, title);

    if (found_movie == nullptr) {cout << "Movie not found." << endl; return;}
    if (found_movie->quantity == 0) {cout << "Movie out of stock." << endl; return;}

    cout << "Movie has been rented." << endl;
    found_movie->quantity--;
    cout << "Movie " << "'" << found_movie->title << "' Info:" <<endl;
    cout << "================================" << endl;
    cout << "Ranking : " << found_movie->ranking << endl;
    cout << "Title   : " << found_movie->title << endl;
    cout << "Year    : " << found_movie->year << endl;
    cout << "Quantity: " << found_movie->quantity << endl;
    return;

}
