#include "HW4-Todo-StackLinkedList.hpp"
#include <iostream>
using namespace std;

TodoStackLinkedList::TodoStackLinkedList() {
  stackHead = nullptr;
}
TodoStackLinkedList::~TodoStackLinkedList() {
  while (!isEmpty()) pop();
}
bool TodoStackLinkedList::isEmpty() {
  return stackHead == nullptr;
}
void TodoStackLinkedList::push(string todoItem) {
  TodoItem* item = new TodoItem;
  item->todo = todoItem;
  item->next = stackHead;
  stackHead = item;

}
void TodoStackLinkedList::pop() {
  if (!isEmpty()) {
    TodoItem* temp = stackHead;
    stackHead = stackHead->next;
    delete temp;
  }
  else cout << "Stack empty, cannot pop an item." << endl;
}
TodoItem* TodoStackLinkedList::peek() {
  if (isEmpty()) {
    cout << "Stack empty, cannot peek." << endl;
    return nullptr;
  }
  return stackHead;
}
