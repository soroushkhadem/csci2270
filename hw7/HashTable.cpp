#include <iostream>
#include <sstream>
#include <fstream>
#include "HashTable.hpp"

using namespace std;

HashTable::HashTable(int hashTableSize) {
    this->hashTableSize = hashTableSize;
    hashTable = new wordItem * [hashTableSize];
    for (int i=0;i<hashTableSize;i++) hashTable[i] = nullptr;
    numItems = 0;
    numCollisions = 0;
}

HashTable::~HashTable() {
    wordItem* temp;
    wordItem* other_temp;
    for (int i = 0; i<hashTableSize; i++) {
        temp = hashTable[i];
        while(temp) {
            other_temp = temp->next;
            delete temp;
            temp = other_temp;
        }
        hashTable[i] = nullptr;
    }
    delete [] hashTable;

}

unsigned int HashTable::getHash(string word) {
    unsigned int hash = 5381;
    for (unsigned int i=0; i<word.length(); i++) hash = hash*33 + word[i];
    return hash % hashTableSize;
}

wordItem* HashTable::searchTable(string word){
    int index = getHash(word);
    wordItem * temp = hashTable[index];
    while(temp) {
        if (temp->word == word) return temp;
        temp = temp->next;
    }
    return nullptr;
}

bool HashTable::isInTable(string word) {
    wordItem * _word = searchTable(word);
    return _word ?  true : false;
}


void HashTable::incrementCount(string word) {
    wordItem * existing = searchTable(word);
    if (existing) existing->count++;
}


void HashTable::addWord(string word) {
    wordItem * existing = searchTable(word);
    if (!existing) {
        numItems++;
        int index = getHash(word);
        wordItem * nw = new wordItem;
        nw->word = word;
        nw->count = 1;
        nw->next = hashTable[index];


        if (hashTable[index] != nullptr) numCollisions++;

        hashTable[index] = nw;


    }
    else {
        existing->count++;
    }
}


int insertIntoSortedArray(wordItem * myArray[], int numEntries, wordItem* word) {

  int elementSpot=-1;

  for (int i=0; i<numEntries; i++) {
    if (word->count >= myArray[i]->count) {
      elementSpot = i;
      break;
    }
  }

  if (elementSpot == -1) {
    elementSpot = numEntries;
  }


  for (int i=numEntries;i>elementSpot;i--) {
    myArray[i] = myArray[i-1];
  }

  myArray[elementSpot] = word;

  numEntries++;
  return numEntries;
}

void HashTable::printTopN(int n) {
    int total_items = numItems + numCollisions;
    wordItem ** wordArray = new wordItem * [total_items];
    int count = 0;
    for(int i=0; i<hashTableSize; i++) {
        wordItem * temp = hashTable[i];
        while (temp) {
            count = insertIntoSortedArray(wordArray, count, temp);
            temp = temp->next;
        }
    }

    for(int i = 0; i<n; i++) {
        cout << wordArray[i]->count << " - " << wordArray[i]->word << endl;
    }

    delete [] wordArray;

}


int HashTable::getNumCollisions() {
    return numCollisions;
}
int HashTable::getNumItems() {
    return numItems;
}
int HashTable::getTotalNumWords(){
    int total = 0;
    for(int i=0; i<hashTableSize; i++) {
        wordItem * temp = hashTable[i];
        while (temp) {
            total += temp-> count;
            temp = temp->next;
        }
    }

    return total;
}

bool isStopWord(string word, HashTable &stopWordsTable) {
    return stopWordsTable.isInTable(word);
}

void getStopWords(char *ignoreWordFileName, HashTable &stopWordsTable) {
    ifstream inFile;
    string input;
    string data;

    inFile.open(ignoreWordFileName);
    if (inFile.is_open()) {
      while(getline(inFile, data)) {
          stringstream ss(data);
          ss >> input;
          stopWordsTable.addWord(input);

      }
    }

}
