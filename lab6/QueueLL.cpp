#include <iostream>
#include "QueueLL.hpp"

using namespace std;

QueueLL::QueueLL()
{
  queueFront = nullptr;
  queueEnd = nullptr;
}

QueueLL::~QueueLL()
{
  while(!isEmpty()) dequeue();
}

bool QueueLL::isEmpty()
{
    return queueFront == nullptr;
}

void QueueLL::enqueue(int key)
{
  Node* temp = new Node;
  temp->key = key;
  temp->next = nullptr;
  if (isEmpty()){
    queueFront = temp;
    queueEnd = temp;
  }
  else {
    queueEnd->next = temp;
    queueEnd = temp;
  }
}

void QueueLL::dequeue()
{
  if (!isEmpty()) {
    Node* temp = queueFront;
    queueFront = queueFront->next;
    if (queueFront == nullptr) queueEnd=queueFront;
    delete temp;
  }
}

int QueueLL::peek()
{
  if (isEmpty()) return 0;
  return queueFront->key;
}
