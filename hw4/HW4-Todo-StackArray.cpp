#include "HW4-Todo-StackArray.hpp"
#include <iostream>
using namespace std;

TodoStackArray::TodoStackArray() {
    stackTop = -1;
}

bool TodoStackArray::isEmpty(){
    return stackTop == -1;
}

bool TodoStackArray::isFull(){
    return stackTop == MAX_STACK_SIZE - 1;
}

void TodoStackArray::push(string todoItem) {
    if (isFull()) {
        cout<< "Stack full, cannot add new todo item." <<endl;
        return;
    }
    TodoItem* item = new TodoItem;
    item->todo = todoItem;
    stackTop++;
    stack[stackTop] = item;
}

void TodoStackArray::pop(){
    if (isEmpty()) {
        cout<< "Stack empty, cannot pop an item." <<endl;
        return;
    }
    TodoItem* to_delete = stack[stackTop];
    delete to_delete;
    stackTop--;
}

TodoItem* TodoStackArray::peek(){
    if (isEmpty()) {
        cout<< "Stack empty, cannot peek." <<endl;
        return NULL;
    }
    return stack[stackTop];
}
