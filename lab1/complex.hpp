// file Complex.hpp
#ifndef COMPLEX_HPP
#define COMPLEX_HPP

class Complex
{
  private:
     double real;
     double imag;
  public:
	Complex( );
 	void setData(double _real, double _imag);
 	void printComplexNumber();

  // getters
  double getReal();
  double getImag();
  // setters
  void setReal(double _real);
  void setImag(double _imag);

  // methods
  Complex add(Complex &complexOther);
  Complex subtract(Complex &complexOther);
  Complex multiply(Complex &complexOther);
  Complex divide(Complex &complexOther);

};
#endif
