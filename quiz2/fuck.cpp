#include <iostream>
using namespace std;

int recur(int n) {
  cout<<n<<",";
  if (n<=1) return n;
  return recur(n-1) + recur(n-2);

}

int doSomething(int a, int b)
 {
     if (b<=1)
     {
         return a - 2;
     }
     else
     {
         return a + doSomething(a, b - 1);
     }
 }

int main() {
  int returnVal = recur(3);
  cout<<"Returning:"<<returnVal<<endl;
  cout << doSomething(33,5) << endl;
  return 0;
}
