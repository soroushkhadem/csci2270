#include <iostream>
#include <climits>
#include <list>
#include <vector>
#include "Graph.hpp"


Graph::Graph() {

}

Graph::~Graph() {

}

void Graph::addEdge(std::string v1, std::string v2, int distance) {
    vertex* vertex1 = findVertex(v1);
    vertex* vertex2 = findVertex(v2);

    // check that they exist
    if (!vertex1) { std::cout << "Error! No existing vertex " << v1 << "\n"; return;};
    if (!vertex2) { std::cout << "Error! No existing vertex " << v2 << "\n"; return;};

    adjVertex adj {vertex2, distance};
    vertex1->adj.push_back(adj);

}

void Graph::addVertex(std::string name) {
    vertex v;
    v.ID = vertices.size();
    v.name = name;
    v.district = 0;
    v.visited = false;

    vertices.push_back(v);
}

// std::distance(adj, vertex.adj.end())

void Graph::displayEdges() {
    // printing
    for (auto const& vertex: vertices) {
        std::cout << vertex.district << ":" << vertex.name ;
        if (vertex.adj.size() > 0) std::cout << "-->"; // city

        // adjacent ones
        unsigned int count = 1; // have to keep track for stupid printing
        for (auto const& adj: vertex.adj) {
            std::cout << adj.v->name ;
            std::cout << " (" << adj.distance << " miles)";
            if (vertex.adj.size()>1 && count != vertex.adj.size()) std::cout << "***";
            count++;
        }
        std::cout << "\n";
    }
}

void Graph::assignDistricts() {
    // loop through the unvisited ones and BFT them to assign distric id's

    int curr_dist = 1;
    for (auto & vertex: vertices) {
        if (!vertex.visited) {
            BFTraversalLabel(vertex.name, curr_dist);
            curr_dist++;
        }
    }




}

vertex* Graph::findVertex(std::string name) {
    for (auto & vertex: vertices) {
        if (vertex.name == name) return &vertex;
    }
    return NULL;
}

void Graph::BFTraversalLabel(std::string startingCity, int distID) {
    vertex* v = findVertex(startingCity);
    v->district = distID;

    std::list<vertex*> queue;
    queue.push_back(v);

    while (!queue.empty()) {
        v = queue.front();
        queue.pop_front();
        for (auto & adj: v->adj) {
            if(!adj.v-> visited) {
                adj.v-> visited = true;
                adj.v-> district = distID;
                queue.push_back(adj.v);
            }
        }
    }
}
