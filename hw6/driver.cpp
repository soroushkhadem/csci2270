#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "MovieTree.hpp"

using namespace std;

void usage(string name){
    cout << "Usage: "<< name <<" <filename.txt>" << endl;
}

void main_menu() {
    cout << "======Main Menu======" << endl;
    cout << "1. Find a movie" << endl;
    cout << "2. Rent a movie" << endl;
    cout << "3. Print the inventory" << endl;
    cout << "4. Delete a movie" << endl;
    cout << "5. Count movies" << endl;
    cout << "6. Quit" << endl;

}

void find_movie(MovieTree* tree) {
    cout << "Enter title:" << endl;
    string input;
    cin.ignore();
    getline(cin, input);
    tree->findMovie(input);
}

void rent_movie(MovieTree* tree) {
    cout << "Enter title:" << endl;
    string input;
    cin.ignore();
    getline(cin, input);
    tree->rentMovie(input);
}

void print_tree(MovieTree tree) {
    tree.printMovieInventory();
}

int main(int argc, char *argv[]) {
    if (argc!=2) {
        usage(argv[0]);
        return 1;
    }

    char *filename = argv[1];
    ifstream inFile;

    inFile.open(filename);
    if (!inFile.is_open()) {
        cout << "Error opening file\n";
        return 1;
    }

    string data;
    int ranking;
    string title;
    int year;
    int quantity;

    static MovieTree main_tree;

    while (getline(inFile,data)) {
        stringstream ss(data);

        getline(ss, data, ',');
        ranking = stoi(data);
        getline(ss, data, ',');
        title = data;
        getline(ss, data, ',');
        year = stoi(data);
        getline(ss, data, ',');
        quantity = stoi(data);

        main_tree.addMovieNode(ranking, title, year, quantity);


    }

    inFile.close();

    int input;
    string name;
    while(1) {
        main_menu();
        cin >> input;
        switch (input) {
            case 1:
                // find_movie(&main_tree);

                cout << "Enter title:"<< endl;
                cin.ignore();
                getline(cin, name);
                main_tree.findMovie(name);
                break;

            case 2:
                // rent_movie(&main_tree);

                cout << "Enter title:"<< endl;
                cin.ignore();
                getline(cin, name);
                main_tree.rentMovie(name);
                break;

            case 3:
                print_tree(main_tree);
                break;
            case 4:
                cout << "Enter title:"<< endl;
                cin.ignore();
                getline(cin, name);
                main_tree.deleteMovie(name);
                break;
            case 5:
                main_tree.countMovies();
            case 6:
                cout << "Goodbye!" << endl;
                return 0;
            default:
                cout << "Incorrect option" << endl;
                cin.clear();
                cin.ignore();
                break;
        }
    }

    //
    // main_tree.printMovieInventory();
    // main_tree.findMovie("The Matrix");
    // main_tree.rentMovie("The Matrix");
    return 0;

}
