#include <iostream>
#include <climits>
#include "PriorityQueue.hpp"

PriorityQueue::PriorityQueue(int _queueSize) {
    currentQueueSize = 0;
    maxQueueSize = _queueSize;
    priorityQueue = new PatientNode[_queueSize + 1];
}
PriorityQueue::~PriorityQueue() {
    delete [] priorityQueue;
}

// for debugging
// void PriorityQueue::printQueue() {
//     for (int i=1; i<currentQueueSize+1; i++) std::cout << i << ":" << priorityQueue[i].name << "\n";
// }


// stupid ones
bool PriorityQueue::isFull() {
    return currentQueueSize == maxQueueSize;;
}
bool PriorityQueue::isEmpty() {
    return !currentQueueSize;
}
std::string PriorityQueue::peekName() {
    if (!isEmpty()) return priorityQueue[1].name;
    return "";
}
int PriorityQueue::peekInjurySeverity() {
    if (!isEmpty()) return priorityQueue[1].injurySeverity;
    return -1;
}
int PriorityQueue::peekTreatmentTime() {
    if (!isEmpty()) return priorityQueue[1].treatmentTime;
    return -1;
}

// add
void PriorityQueue::enqueue (std::string _name, int _injurySeverity, int _treatmentTime) {
    if (isFull()) return;

    PatientNode patient;
    patient.name = _name;
    patient.injurySeverity = _injurySeverity;
    patient.treatmentTime = _treatmentTime;

    currentQueueSize++;
    priorityQueue[currentQueueSize] = patient;
    repairUpward(currentQueueSize);

}

// take off top
void PriorityQueue::dequeue() {
    if (currentQueueSize<=0) return;
    if (currentQueueSize == 1) { currentQueueSize--; return; }
    priorityQueue[1] = priorityQueue[currentQueueSize];
    currentQueueSize--;
    repairDownward(1);
}


// priority goes to least wait time, then least tratment time
bool shouldSwap(PatientNode first, PatientNode second) {
    if (first.injurySeverity > second.injurySeverity) return false;
    if (first.injurySeverity < second.injurySeverity) return true;
    if (first.treatmentTime > second.treatmentTime) return true;
    return false;
}

// swaps if it should
bool swap(PatientNode *first, PatientNode *second) {
    if (shouldSwap(*first, *second)) {
        PatientNode temp = *first;
        *first = *second;
        *second = temp;
        return false;
    }
    return true;
}

// starts at end and swaps to create a correct binary heap
void PriorityQueue::repairUpward(int nodeIndex) {
    int parent = nodeIndex / 2;
    if (parent != 0 && !swap(&priorityQueue[parent], &priorityQueue[nodeIndex])) repairUpward(parent);
}

// just swaps
void blindSwap(PatientNode *first, PatientNode *second) {
    PatientNode temp = *first;
    *first = *second;
    *second = temp;
}
 // starts at top and swaps in order to create correct binary heap
void PriorityQueue::repairDownward(int nodeIndex) {
    int l = nodeIndex * 2;
    int r = l + 1;
    int smallest = nodeIndex;

    if (l<=currentQueueSize && shouldSwap(priorityQueue[smallest], priorityQueue[l])) smallest = l;
    if (r<=currentQueueSize && shouldSwap(priorityQueue[smallest], priorityQueue[r])) smallest = r;

    if (smallest != nodeIndex) {
        blindSwap(&priorityQueue[smallest], &priorityQueue[nodeIndex]);
        repairUpward(smallest);
    }

}
