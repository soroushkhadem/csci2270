#include "HW4-Todo-QueueLinkedList.hpp"
#include <iostream>
using namespace std;

TodoQueueLinkedList::TodoQueueLinkedList() {
  queueFront = nullptr;
  queueEnd = nullptr;
}
TodoQueueLinkedList::~TodoQueueLinkedList() {
  while (!isEmpty()) dequeue();
}
bool TodoQueueLinkedList::isEmpty() {
  return queueFront == nullptr;
}
void TodoQueueLinkedList::enqueue(string todoItem) {
  TodoItem* item = new TodoItem;
  item->todo = todoItem;
  item->next = nullptr;
  if(isEmpty()) {
    queueFront = item;
    queueEnd = item;
  }
  else {
    queueEnd->next = item;
    queueEnd = item;
  }
}
void TodoQueueLinkedList::dequeue() {
  if (!isEmpty()) {
    TodoItem* temp = queueFront;
    queueFront = queueFront->next;
    if (queueFront == nullptr) queueEnd=queueFront;
    delete temp;
  }
  else cout << "Queue empty, cannot dequeue an item." <<endl;
}
TodoItem* TodoQueueLinkedList::peek(){
  if (isEmpty()) {
    cout << "Queue empty, cannot peek." <<endl;
    return nullptr;
  }
  return queueFront;
}
