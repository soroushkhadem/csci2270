#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

int readFile(string fileName);

int main (int argc, char* argv[]) {
  readFile("input.txt");
}

int readFile(string fileName) {
  ifstream inFile;
  string data;

  inFile.open(fileName);
    if (inFile.is_open()) {
    string input;
    while(getline(inFile, data)) {
      stringstream ss(data);

      ss >> input;
      cout << input << endl;
    }
    inFile.close();
  }
  else {
    cout << "Error opening file" << endl;
    return 1;
  }

  return 0;
}
