#include "HW4-Todo-QueueArray.hpp"
#include <iostream>
using namespace std;

TodoQueueArray::TodoQueueArray() {
    queueFront = -1;
    queueEnd = -1;
}
bool TodoQueueArray::isEmpty() {
    return queueFront == -1;
}
bool TodoQueueArray::isFull() {
    return (queueEnd+1)%MAX_QUEUE_SIZE == queueFront;
}
void TodoQueueArray::enqueue(string todoItem) {
    if (isFull()) {
        cout<<"Queue full, cannot add new todo item." <<endl;
        return;
    }
    TodoItem* item = new TodoItem;
    item->todo = todoItem;
    if (queueFront == -1) {
        queueFront = queueEnd = 0;
    }
    else queueEnd = (queueEnd+1)%MAX_QUEUE_SIZE;
    queue[queueEnd] = item;

}
void TodoQueueArray::dequeue() {
    if (isEmpty()) {
        cout<<"Queue empty, cannot dequeue an item." <<endl;
        return;
    }
    TodoItem* to_delete = queue[queueFront];
    delete to_delete;
    if (queueFront == queueEnd) {
        queueFront = -1;
        queueEnd = -1;
    }
    else queueFront = (queueFront+1)%MAX_QUEUE_SIZE;

}
TodoItem* TodoQueueArray::peek() {
    if (isEmpty()) {
        cout<<"Queue empty, cannot peek." <<endl;
        return NULL;
    }
    return queue[queueFront];
}
