// ==========================================
// Created: August 23, 2018
// @author Soroush
//
// Description: Counts unique words in a file
// outputs the top N most common words
// ==========================================

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

// struct to store word + count combinations
struct wordItem
{
    string word;
    int count;
};

/*
* Function name: getStopWords
* Purpose: read stop word list from file and store into vector
* @param ignoreWordFile - filename (the file storing ignore/stop words)
* @param _vecIgnoreWords - store ignore words from the file (pass by reference)
* @return - none
* Note: The number of words is fixed to 50
*/
void getStopWords(const char *ignoreWordFileName, vector<string> &_vecIgnoreWords);

/*
* Function name: isStopWord
* Purpose: to see if a word is a stop word
* @param word - a word (which you want to check if it is a stop word)
* @param _vecIgnoreWords - the vector type of string storing ignore/stop words
* @return - true (if word is a stop word), or false (otherwise)
*/
bool isStopWord(string word, vector<string> &_vecIgnoreWords);

/*
* Function name: getTotalNumberNonStopWords
* Purpose: compute the total number of words saved in the words array (including repeated words)
* @param list - an array of wordItems containing non-stopwords
* @param length - the length of the words array
* @return - the total number of words in the words array (including repeated words multiple times)
*/
int getTotalNumberNonStopWords(wordItem list[], int length);

/*
* Function name: arraySort
* Purpose: sort an array of wordItems, decreasing, by their count fields
* @param list - an array of wordItems to be sorted
* @param length - the length of the words array
*/
void arraySort(wordItem list[], int length);

/**
* Function name: printTopN
* Purpose: to print the top N high frequency words
* @param wordItemList - a pointer that points to a *sorted* array of wordItems
* @param topN - the number of top frequency words to print
* @return none
*/
void printTopN(wordItem wordItemList[], int topN);

const int STOPWORD_LIST_SIZE = 50;

const int INITIAL_ARRAY_SIZE = 100;

// ./a.out 10 HW2-HungerGames_edit.txt HW2-ignoreWords.txt
int main(int argc, char *argv[])
{
    vector<string> vecIgnoreWords(STOPWORD_LIST_SIZE);

    // verify we have the correct # of parameters, else throw error msg & return
    if (argc != 4)
    {
        cout << "Usage: ";
        cout << argv[0] << " <number of words> <filename.txt> <ignorefilename.txt>";
        cout << endl;
        return 0;
    }

    /* **********************************
    1. Implement your code here.

    Read words from the file passed in on the command line, store and
    count the words.

    2. Implement the six functions after the main() function separately.
    ********************************** */

    // get command line args
    char *stop_words_file, *main_file;
    int num_words;

    num_words = stoi(argv[1]);
    main_file = argv[2];
    stop_words_file = argv[3];

    // get and store the stop words
    getStopWords(stop_words_file, vecIgnoreWords);

    // read in words and add to array of unique words if not in stop list
    ifstream inFile;
    string input_word;

    int wordArray_size = 100;
    wordItem * wordArray;
    wordArray = new wordItem[wordArray_size]; // allocate new array

    int last_entered = 0;
    int times_doubled = 0;

    inFile.open(main_file);
    if(inFile.is_open()) {
        while (inFile >> input_word) {

            // cout << input_word << endl;

            if (isStopWord(input_word, vecIgnoreWords)) continue; // ignored

            int i;

            for (i=0;i<wordArray_size;i++) {
                if (wordArray[i].word == input_word) break;
            }


            if (i == wordArray_size){ // word was not in array, insert at end
                wordItem new_word;
                new_word.word = input_word;
                new_word.count = 1;

                wordArray[last_entered] = new_word;
                last_entered ++;
            }
            else { // word is in array, modfiy count
                wordArray[i].count++;
            }


            if (last_entered == wordArray_size) { // reached the end of our array, let's double

                // point to old  array
                wordItem * tempArray;
                tempArray = wordArray;

                // double and then copy
                wordArray = new wordItem[wordArray_size*2];
                // copy into temp array
                for (int j=0; j<wordArray_size; j++) wordArray[j] = tempArray[j];

                // free old array
                delete [] tempArray;

                wordArray_size *= 2;
                times_doubled++;
            }


        }

        // for now just print whole array

    }


    // sort our array
    arraySort(wordArray, last_entered);
    // for (int j=0;j<last_entered;j++) {
    //     cout << wordArray[j].word << ": " << wordArray[j].count << '\n';
    // }

    // start printing
    printTopN(wordArray, num_words);
    cout << "#" << endl;
    cout << "Array doubled: " << times_doubled << endl;
    cout << "#" << endl;
    cout << "Unique non-stop words: " << last_entered  << endl;
    cout << "#" << endl;
    cout << "Total non-stop words: " << getTotalNumberNonStopWords(wordArray, last_entered) << endl;


    // delete our array
    delete [] wordArray;

    return 0;
}

void getStopWords(const char *ignoreWordFileName, vector<string> &_vecIgnoreWords)
{
  ifstream inFile;
  string input;
  string data;

  inFile.open(ignoreWordFileName);
  if (inFile.is_open()) {
    while(getline(inFile, data)) {
      stringstream ss(data);
      ss >> input;

      _vecIgnoreWords.push_back(input);
    }
  }

}

bool isStopWord(string word, vector<string> &_vecIgnoreWords)
{
    unsigned int i;
    for(i=0;i<_vecIgnoreWords.size(); i++){
        if (_vecIgnoreWords[i] == word) return true;
    }
    return false;
}

int getTotalNumberNonStopWords(wordItem list[], int length)
{
    int sum = 0;
    for (int i = 0; i<length; i++) sum += list[i].count;
    return sum;
}

void arraySort(wordItem list[], int length)
{
    // bubble sort algorithim
    for (int i=0; i<length-1; i++) {
        for(int j = 0; j<length -i -1; j++) {
            if (list[j].count < list[j+1].count) {
                wordItem temp = list[j+1];
                list[j+1] = list[j];
                list[j] = temp;
            }
        }

    }
}

void printTopN(wordItem wordItemList[], int topN)
{
    for (int i=0; i<topN; i++) {
        cout << wordItemList[i].count << " - " << wordItemList[i].word << endl;
    }
}
