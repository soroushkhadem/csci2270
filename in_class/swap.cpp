#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

int swap(int &a, int &b);

int main () {
  int a = 19;
  int b = 22;

  swap(a, b);

  cout << a << endl;
  cout << b << endl;

}

int swap(int &a, int &b) {
  int c = 0;
  c = b;
  b = a;
  a = c;

  return 0;

}
