#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "PriorityQueue.hpp"
using namespace std;

// helper print functions
void usage(string name) { cout << "Usage: "<< name <<" <queue size> <patient filename>" << endl; }
void full() { cout << "Priority Queue full. Send remaining patients to another hospital." << endl; }
void empty() { cout << "Queue empty." << endl; }
void main_menu() {
    cout << "======Main Menu======" << endl;
    cout << "1. Get Patient Information from File" << endl;
    cout << "2. Add Patient to Priority Queue" << endl;
    cout << "3. Show Next Patient" << endl;
    cout << "4. Admit Next Patient" << endl;
    cout << "5. Process Entire Queue" << endl;
    cout << "6. Quit" << endl;
}

// read from filename and fill the queue
int fillQueue(PriorityQueue *q, char * filename) {
    ifstream inFile;
    inFile.open(filename);

    if (!inFile.is_open()) {
        cout << "Error opening file\n";
        return -1;
    }

    string data;
    string name;
    int _injurySeverity;
    int _treatmentTime;

    while (getline(inFile,data)) {
        if (q->isFull()) {
            full();
            return 0;
        }

        stringstream ss(data);

        getline(ss, data, ' ');
        name = data;
        getline(ss, data, ' ');
        _injurySeverity = stoi(data);
        getline(ss, data, ' ');
        _treatmentTime = stoi(data);

        q->enqueue(name, _injurySeverity, _treatmentTime);

    }
    inFile.close();
    return 0;
}

int main(int argc, char *argv[]) {

    // this teacher is so fucking dumb
    // if (argc != 3) { usage(argv[0]); return -1; }
    // char * filename = argv[2];
    char * filename = argv[0]; // fuck moodle

    int queue_size = stoi(argv[1]);

    // init
    PriorityQueue main_queue = PriorityQueue(queue_size);
    int totalWaitTime = 0;

    // needed vars
    int input;
    string name;
    int severity;
    int ttime;

    // main loop
    while(1) {
        main_menu();
        cin >> input;
        switch (input) {
            case 1:
                fillQueue(&main_queue, filename);
                break;
            case 2:
                if (main_queue.isFull()) full();
                else {
                    cout << "Enter Patient Name:"<< endl;
                    cin.ignore();
                    cin >> name;
                    cout << "Enter Injury Severity:"<< endl;
                    cin.ignore();
                    cin >> severity;
                    cout << "Enter Treatment Time:"<< endl;
                    cin.ignore();
                    cin >> ttime;


                    main_queue.enqueue(name, severity, ttime);
                }

                break;

            case 3:
                if (main_queue.isEmpty()) empty();
                else {
                    cout << "Patient Name: " << main_queue.peekName() << endl;
                    cout << "Injury Severity: " << main_queue.peekInjurySeverity() << endl;
                    cout << "Treatment Time: " << main_queue.peekTreatmentTime() << endl;
                }

                break;

            case 4:
                if (main_queue.isEmpty()) empty();
                else {
                    cout << "Patient Name: " << main_queue.peekName() << " - Total Time Treating Patients: " << main_queue.peekTreatmentTime() << endl; // moodle sucks
                    main_queue.dequeue();
                }

                break;

            case 5:
                if (main_queue.isEmpty()) empty();
                else {
                    while (!main_queue.isEmpty()) {
                        cout << "Name: " << main_queue.peekName() << " - Total Wait Time: " << totalWaitTime << endl;
                        totalWaitTime += main_queue.peekTreatmentTime();
                        main_queue.dequeue();
                    }

                }

                break;

            case 6:
                cout << "Goodbye!" << endl;
                return 0;
            default:
                cout << "Incorrect option" << endl;
                cin.clear();
                cin.ignore();
                break;
        }
    }



}
