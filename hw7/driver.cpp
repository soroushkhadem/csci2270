#include <string>
#include <fstream>
#include <iostream>

#include "HashTable.hpp"

using namespace std;

int main(int argc, char *argv[]) {
    if (argc != 5)
    {
        cout << "Usage: ";
        cout << argv[0] << " <number of words> <filename.txt> <ignorefilename.txt> <hash table size>";
        cout << endl;
        return 0;
    }

    int n = stoi(argv[1]);
    string main_file = argv[2];
    char * stop_file = argv[3];
    int table_size = stoi(argv[4]);

    HashTable stop_words(STOPWORD_TABLE_SIZE);
    HashTable all_words(table_size);

    getStopWords(stop_file, stop_words);
    
    ifstream inFile;
    string input_word;
    inFile.open(main_file);
    if(inFile.is_open()) {
        while (inFile >> input_word) {
            if (!isStopWord(input_word, stop_words)) {
                if (all_words.isInTable(input_word)) {
                    all_words.incrementCount(input_word);
                }
                 else {
                    all_words.addWord(input_word);
                 }
                
            }
            
                
            
        }
        inFile.close();
    }
    
    

    all_words.printTopN(n);

    cout << "#" << endl;
    cout << "Number of collisions: " << all_words.getNumCollisions() << endl;
    cout << "#" << endl;
    cout << "Unique non-stop words: " << all_words.getNumItems() << endl;
    cout << "#" << endl;
    cout << "Total non-stop words: " << all_words.getTotalNumWords() << endl;


}

