#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

class Movie {
  string title;
  int year;
  double rating;
public:
  Movie() {
    title = "unknown";
    year = 2016;
    rating = 0.0;
  }
  Movie(string _title, int _year, double _rating) {
    title = _title;
    year = _year;
    rating = _rating;
  }
  string getTitle() {
    return title;
  }
  int getYear() {
    return year;
  }
  double getRating() {
    return rating;
  }

  void setTitle(string _title) {
    title = _title;
  }
  void setYear(int _year) {
    year= _year;
  }
  void setRating(double _rating) {
    rating = _rating;
  }

  void displayMovie() {
    cout << "Title: " << title << endl;
    cout << "Year: " << year << endl;
    cout << "Rating: " << rating << endl;
  }

};

int main(int argc, char* argv[]) {
  Movie movie1;
  movie1.displayMovie();
  movie1.setTitle("Hello");
  movie1.setYear(2018);
  movie1.setRating(5.0);
  movie1.displayMovie();
  Movie movie2("Hi",1999,1.0);
  movie2.displayMovie();
}
