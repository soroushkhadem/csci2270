#include <iostream>
using namespace std;

struct Node {
  int val;
  Node *previous;
  Node *next;
};

class DLL {
private:
  Node* head;
  Node* tail;
public:
  DLL() {
    head = NULL;
    tail = NULL;
  };
  void append(int val){
    Node* node = new Node;
    node->val = val;
    node->previous = NULL;
    node->next = NULL;

    if (head == NULL && tail == NULL) {
      head = node;
      tail = node;
      return;
    }

    node->previous = tail;
    tail->next = node;
    tail = node;
    return;

  }

  void insertAt(int index, int val) {
    int pos=0;
    Node *curr = head;
    while (pos<index && curr->next!=NULL) {
      curr = curr -> next;
      pos ++;
    }

    Node* node = new Node;
    node->val = val;
    node->previous = curr->previous;
    node->next = curr;

    if (node->previous) node->previous->next = node;
    else head = node;

    if (node->next) node->next->previous = node;
    else tail = node;

  }

  void print(){
    Node* ptr = head;
    while (ptr!=NULL) {
      cout << ptr->val << '\n';
      ptr = ptr->next;
    }
  }
  void printReverse(){
    Node *ptr = tail;
    while (ptr!=NULL) {
      cout << ptr->val << '\n';
      ptr = ptr->previous;
    }
  }
};


int main(int argc, char* argv[]) {
  DLL list;
  list.append(0);
  list.append(1);
  list.append(2);
  list.print();
  cout << '\n';
  list.insertAt(1, 10);
  list.print();

  return 0;
}
