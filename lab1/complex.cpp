// file Complex.cpp
#include "complex.hpp"
#include <iostream>

Complex::Complex()
{

}

void Complex::setData(double _real, double _imag)
{
  real = _real;
  imag = _imag;
}
void Complex::printComplexNumber()
{
   std::cout << "(" << real << "," << imag << "i)" << std::endl;
}

double Complex::getReal()
{
  return real;
}

double Complex::getImag()
{
  return imag;
}

void Complex::setReal(double _real)
{
  real = _real;
}

void Complex::setImag(double _imag)
{
  imag = _imag;
}

Complex Complex::add(Complex &complexOther)
{
  Complex complexSum;
  complexSum.real = real + complexOther.getReal();
  complexSum.imag = imag + complexOther.getImag();
  return complexSum;
}

Complex Complex::subtract(Complex &complexOther)
{
  Complex complexSum;
  complexSum.real = real - complexOther.getReal();
  complexSum.imag = imag - complexOther.getImag();
  return complexSum;
}
Complex Complex::multiply(Complex &complexOther)
{
  Complex complexSum;
  complexSum.real = (real * complexOther.getReal()) - (imag * complexOther.getImag());
  complexSum.imag = (real * complexOther.getImag()) + (imag * complexOther.getReal());
  return complexSum;
}
Complex Complex::divide(Complex &complexOther)
{
  Complex complexSum;
  complexSum.real = ((real * complexOther.getReal()) + (imag * complexOther.getImag()))/(complexOther.getReal() * complexOther.getReal() + complexOther.getImag() * complexOther.getImag());
  complexSum.imag = (imag * complexOther.getReal()) - (real * complexOther.getImag()) / (complexOther.getImag() * complexOther.getImag() + complexOther.getImag() * complexOther.getImag());
  return complexSum;
}
