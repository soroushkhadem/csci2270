#include "MovieTree.hpp"
#include <iostream>
#include <string>

using namespace std;

MovieTree::MovieTree(){
    root = nullptr;
}

void destructMovie(MovieNode * currNode){
  if (currNode!=NULL){
    destructMovie(currNode->leftChild);
    destructMovie(currNode->rightChild);
    delete currNode;
    currNode=NULL;
  }
}

MovieTree::~MovieTree(){
  destructMovie(root);
}

MovieNode* addMovieHelper(MovieNode* curr, int ranking, string title, int releaseYear, int quantity) {
    if (curr == nullptr) return new MovieNode(ranking, title, releaseYear, quantity);

    if (title.compare(curr->title) < 0) curr->leftChild = addMovieHelper(curr->leftChild, ranking, title, releaseYear, quantity);
    else curr->rightChild = addMovieHelper(curr->rightChild, ranking, title, releaseYear, quantity);

    return curr;
}

void MovieTree::addMovieNode(int ranking, string title, int releaseYear, int quantity) {

    if(root==nullptr) {
        root = new MovieNode(ranking, title, releaseYear, quantity);
        return;
    }

    root = addMovieHelper(root, ranking, title, releaseYear, quantity);

}


void printMoviesHelper(MovieNode* curr){
    if (curr) {
        printMoviesHelper(curr->leftChild);
        cout << "Movie: " << curr->title << " " << curr->quantity << endl;
        printMoviesHelper(curr->rightChild);
    }
}

void MovieTree::printMovieInventory() {
    printMoviesHelper(root);
}

MovieNode* searchHelper(MovieNode* root, string title) {
    if (root == nullptr || title.compare(root->title) == 0) return root;

    if (title.compare(root->title) < 0) return searchHelper(root->leftChild, title);

    return searchHelper(root->rightChild, title);

}

void MovieTree::findMovie(string title) {
    MovieNode* found_movie = searchHelper(root, title);
    if (found_movie == nullptr) {cout << "Movie not found." << endl; return;}

    cout << "Movie " << "'" << found_movie->title << "' Info:" <<endl;
    cout << "================================" << endl;
    cout << "Ranking : " << found_movie->ranking << endl;
	  cout << "Title   : " << found_movie->title << endl;
	  cout << "Year    : " << found_movie->year << endl;
	  cout << "Quantity: " << found_movie->quantity << endl;
    return;

}

void MovieTree::rentMovie(string title) {
    MovieNode* found_movie = searchHelper(root, title);

    if (found_movie == nullptr) {cout << "Movie not found." << endl; return;}
    if (found_movie->quantity == 0) {cout << "Movie out of stock." << endl; return;}

    cout << "Movie has been rented." << endl;
    found_movie->quantity--;
    cout << "Movie " << "'" << found_movie->title << "' Info:" <<endl;
    cout << "================================" << endl;
    cout << "Ranking : " << found_movie->ranking << endl;
    cout << "Title   : " << found_movie->title << endl;
    cout << "Year    : " << found_movie->year << endl;
    cout << "Quantity: " << found_movie->quantity << endl;
    if (found_movie->quantity==0) deleteMovie(found_movie->title);
    return;

}


MovieNode* getMinValueHelper(MovieNode * currNode){
  if(currNode->leftChild==NULL) return currNode;
  return getMinValueHelper(currNode->leftChild);
}

MovieNode* deleteMovieHelper(MovieNode * currNode, string title) {

  if (currNode==NULL) return NULL;

  if (currNode->title.compare(title)>0) currNode->leftChild=deleteMovieHelper(currNode->leftChild,title);

  else if (currNode->title.compare(title)<0) currNode->rightChild=deleteMovieHelper(currNode->rightChild,title);

  else {

    if (currNode->leftChild==NULL and currNode->rightChild ==NULL){
      delete currNode;
      currNode=NULL;
    }
    else if (currNode->leftChild==NULL){
      MovieNode * tmp = currNode;
      currNode=currNode->rightChild;
      delete tmp;
    }
    else if (currNode->rightChild==NULL){
      MovieNode * tmp = currNode;
      currNode=currNode->leftChild;
      delete tmp;
    }
    else{
        MovieNode * min_val = getMinValueHelper(currNode->rightChild);
        currNode->quantity = min_val->quantity;
        currNode->ranking = min_val->ranking;
        currNode->title = min_val->title;
        currNode->rightChild=deleteMovieHelper(currNode->rightChild,min_val->title);
    }
  }
  return currNode;
}

void MovieTree::deleteMovie(string title) {
  if (root!=NULL) root = deleteMovieHelper(root, title);
  else cout << "Movie not found." << endl;
}

int getSizeHelper(MovieNode* currNode) {
  if (currNode == NULL) return 0;

  return getSizeHelper(currNode->leftChild) + getSizeHelper(currNode->rightChild) + 1;
}

void MovieTree::countMovies(){
  cout << "Count = " << getSizeHelper(root) << endl;
}
