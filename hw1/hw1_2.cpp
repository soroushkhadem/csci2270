#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;
#include <vector>

struct student
{
  string username;
  float gpa;
  int age;
};

void addUser(vector<student> &gradeList, string name, float _gpa, int _age);
void printList(const vector<student> &gradeList);

int main (int argc, char* argv[]) {
  if (argc < 2) {
    cout << "Need to enter a filename" << endl;
    return 1;
  }
  // file reading
  ifstream inFile;
  string data;

  vector<student> students;

  // will change dynamically
  string username;
  float gpa;
  int age;

  inFile.open(argv[1]);
    if (inFile.is_open()) {
    while(getline(inFile, data)) {
      stringstream ss(data);
      // parse through ss, reuse data
      getline(ss, data, ',');
      username = data;
      getline(ss, data, ',');
      gpa = stof(data);
      getline(ss, data, ',');
      age = stoi(data);

      addUser(students, username, gpa, age);
      //
      // getline(inFile, data, ',');
      // stringstream sg(data);
      // sg >> gpa;
      // getline(inFile, data, ',');
      // stringstream sa(data);
      // sa >> age;
    }
    inFile.close();

    printList(students);

    // cout << "name " << username << endl;
    // cout << "gpa " << gpa << endl;
    // cout << "age " << age << endl;
  }
  else {
    cout << "Error opening file" << endl;
    return 1;
  }

  return 0;

}

void addUser(vector<student> &gradeList, string name, float _gpa, int _age) {
  student newMember;
  newMember.username = name;
  newMember.gpa = _gpa;
  newMember.age = _age;
  gradeList.push_back(newMember);
}
void printList(const vector<student> &gradeList) {
  for(int i=0;i<gradeList.size();i++) {
    cout << gradeList[i].username <<" ["<<gradeList[i].gpa<<"] age: "<<gradeList[i].age<< endl;
  }
}
