// CPP program to implement hashing with chaining
#include<iostream>
#include "hash.hpp"

using namespace std;

node* HashTable::createNode(string key, node* next)
{
    node* nw = new node;
    nw->key = key;
    nw->next = next;
    return nw;
}

HashTable::HashTable(int bsize)
{
    tableSize = bsize;
    table = new node * [tableSize];
    for (int i =0; i<bsize; i++) table[i] = nullptr;
}

//function to calculate hash function
unsigned int HashTable::hashFunction(string key)
{
    unsigned int hash = 0;

    for (int i=0;i<key.length();i++) hash += key[i];

    return hash % tableSize;
}

//function to search
node* HashTable::searchItem(string key)
{
    // get index and then loop through the linked list at that index in table

    int index = hashFunction(key);
    node * temp = table[index];

    while(temp) {
      if (temp->key == key) return temp;
      temp = temp->next;
    }

    return nullptr;
}

//function to insert
bool HashTable::insertItem(string key)
{
    if (!searchItem(key)) {
          int index = hashFunction(key);
          node * nw = createNode(key, nullptr);
          if (table[index] == nullptr) table[index] = nw;
          else {
             nw->next = table[index];
             table[index] = nw;
          }

          return true;
    }

    return false;
}

// function to delete
bool HashTable::deleteItem(string key)
{
    // int index = hashFunction(key);
    // node *temp = table[index];
    // bool found = false;
    //
    // if (!temp) return false;
    // else if (temp->key == key) {
    //   table[index] = temp->next;
    //   delete temp;
    //   temp = nullptr;
    //   found = true;
    // }
    // else {
    //   while(temp->next && !found) {
    //     if(temp->next->key == key) found = true;
    //     else temp = temp->next;
    //   }
    //
    //   if (found) {
    //     node * del = temp->next;
    //     temp->next = temp->next->next;
    //     delete del;
    //     del = nullptr;
    //   }
    // }
    //
    // return found;


    if (searchItem(key)) {
      int index = hashFunction(key);
      node * temp = table[index];
      while (temp) {
        if (temp->key == key) {
          if (table[index]->next) table[index]->next = temp->next;
          else table[index] = temp->next;
          return true;
        }
        temp = temp->next;
      }
    }
    return false;
}

// function to display hash table
void HashTable::printTable()
{
    for (int i = 0; i < tableSize; i++) {
        cout << i <<"|| ";
        node* temp = table[i];
        while(temp)
        {
            cout<< temp->key;
            if(temp->next!=nullptr)
            {
                cout<<"-->";
            }
            temp = temp->next;
        }
        cout<<endl;

    }
}
