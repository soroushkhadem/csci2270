#include <iostream>
#include "StackLL.hpp"

using namespace std;

StackLL::StackLL()
{
  stackHead = nullptr;
}

StackLL::~StackLL()
{
  while(!isEmpty()) pop();
}

bool StackLL::isEmpty()
{
  return (stackHead==nullptr);
}

void StackLL::push(int key)
{
  Node* temp = new Node;
  temp->key = key;
  temp->next = stackHead;
  stackHead = temp;
}

void StackLL::pop()
{
  if (!isEmpty()) {
    Node* temp = stackHead;
    stackHead = stackHead->next;
    delete temp;
  }
}

int StackLL::peek()
{
  if (isEmpty()) {
    cout << "empty\n" ;
    return 0;
  }

  return stackHead->key;


}
